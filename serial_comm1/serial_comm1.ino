void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //tells program that we are using serial communication SYNTAX: Serial.begin(baudrate);
 
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("HELLO I AM esp12"); //displays outspecified as parameters SYNTAX: Serial.println(parameters); 
  //USE Serial.print(parameters to get output in same line itself
  
}
