void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int x = analogRead(A0); //Reads analog signal from ADC port, converts it to digital and stores in variable x
  Serial.println(x);  //displaying digital value read from ADC port
}
