#include<ESP8266WiFi.h>

WiFiServer server(80);  //Initializing a server SYNTAX: WiFiServer server_name(port_number);  PORT NUMBER: 80 for HTTP comm. Each comm. protocol has its own port no.



void setup() {
  // put your setup code here, to run once:
  server.begin();  //starts the initialized server
  
  WiFi.mode(WIFI_AP); //To change wifi mode of the controller. SYNTAX: WiFi.mode(arguements); 
  /*ARGUEMENTS: 
   * WIFI_AP(To set as Access Point), 
   * WIFI_STA (For setting it as station), 
   * WIFI_AP_STA (For setting it as both access point and station*/
  WiFi.softAP("microcontrollr", "12345678"); //Configuring our WiFi Access Point by giving it a Name and Password SYNTAX: WiFi.softAP(<ssid>,<password>);
  
  Serial.begin(115200); //baud rate of 115200 is generally used for WiFi
  Serial.println(WiFi.softAPIP());  //WiFi.softAPIP() will return the IP address of the access point created on the microcontroller
}

void loop() {
  // put your main code here, to run repeatedly:
  //don't delete loop() even if you are not using it
  WiFiClient clien;
  while((clien = server.available())==0); //details of client is stored to clien when client is connected and the loop checks whether any client is connected
  Serial.println(clien.remoteIP());

   while(clien.available()==0 && clien.connected()==1)
  {
    delay(10);
  }
  String request = clien.readStringUntil('\r');
  Serial.println(request);
  if(request.indexOf("temp")!=-1)
  {
    float temp = analogRead(A0);
    temp = temp/1024.0;
    temp = (temp * 1000)/10;
    clien.println(temp);
  }
  clien.flush();
  
}
