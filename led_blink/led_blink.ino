void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT); //set pin 13 as output pin SYNTAX : pinMode(<pin number> , <INPUT/OUTPUT>)
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);  //set pin 13 as high SYNTAX: digitalWrite(<pin number> , <HIGH/LOW>)
  delay(1000);  //delay function creates a time delay SYNTAX: delay(<time in milliseconds>)
  digitalWrite(13,LOW); //set pin 13 as low
  delay(1000);
}
